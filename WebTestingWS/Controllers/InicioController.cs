﻿using SgpWebApi;
using SgpWebApi.Models.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EL;

namespace WebTestingWS.Controllers
{
    public class InicioController : Controller
    {
        public ActionResult Index()
        {
            RequesUtils requesUtils = new RequesUtils();
            Cnf.usuarios usuarios = new Cnf.usuarios();
            usuarios.codigo = "mmojica";
            usuarios.password = "marvickval";
            Reply reply = requesUtils.Execute<Cnf.usuarios>("http://localhost:49786/api/Login/verificarCredenciales", "post", usuarios);
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
             
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}