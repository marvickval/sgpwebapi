﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL
{
    public class Cnf
    {
        #region ENTIDADES DEL SCHEMA
        public class usuarios
        {
            public usuarios() { }

            public Guid uidregist { get; set; }

            public Guid uidcia { get; set; }

            [Required(ErrorMessage = "El campo código es requerido")]
            public string codigo { get; set; }

            public string codigoopesis { get; set; }

            public string descripci { get; set; }

            public short numerotipaut { get; set; }
            [Required(ErrorMessage = "El campo password es requerido")]
            public string password { get; set; }

            public int numeroobjeto { get; set; }

            public int numeroestado { get; set; }

            public string imagen { get; set; }

            public short yearfiscalusing { get; set; }

            public short mesfiscalusing { get; set; }

            public short lenguajeusing { get; set; }

            public Guid? uidempleado { get; set; }

            public Guid? uidtelevendedor { get; set; }

            public Guid? uidvendedor { get; set; }

            public Guid? uidcobrador { get; set; }

            public Guid? uidcajero { get; set; }

            public DateTime crefch { get; set; }

            public string creusr { get; set; }

            public string crehsn { get; set; }

            public string crehid { get; set; }

            public string creips { get; set; }

            public DateTime modfch { get; set; }

            public string modusr { get; set; }

            public string modhsn { get; set; }

            public string modhid { get; set; }

            public string modips { get; set; }

        }

        public class usuariosrolesrel
        {
            public Guid uidregist { get; set; }

            public Guid uidcia { get; set; }

            public Guid uidusuario { get; set; }

            public Guid uidrole { get; set; }

            public bool indaplica { get; set; }

            public DateTime crefch { get; set; }

            public string creusr { get; set; }

            public string crehsn { get; set; }

            public string crehid { get; set; }

            public string creips { get; set; }

            public DateTime modfch { get; set; }

            public string modusr { get; set; }

            public string modhsn { get; set; }

            public string modhid { get; set; }

            public string modips { get; set; }

            public virtual usuariosroles usuariosroles { get; set; }

            public virtual usuarios usuarios { get; set; }
        }

        public class usuariosroles
        {
            public usuariosroles()
            {
                this.usuariosrolesrel = (ICollection<EL.Cnf.usuariosrolesrel>)new HashSet<EL.Cnf.usuariosrolesrel>();
                this.usuariosrolesasegurablesrel = (ICollection<EL.Cnf.usuariosrolesasegurablesrel>)new HashSet<EL.Cnf.usuariosrolesasegurablesrel>();
            }

            public Guid uidregist { get; set; }

            public Guid uidcia { get; set; }

            public string descripci { get; set; }

            public int numeroobjeto { get; set; }

            public int numeroestado { get; set; }

            public DateTime crefch { get; set; }

            public string creusr { get; set; }

            public string crehsn { get; set; }

            public string crehid { get; set; }

            public string creips { get; set; }

            public DateTime modfch { get; set; }

            public string modusr { get; set; }

            public string modhsn { get; set; }

            public string modhid { get; set; }

            public string modips { get; set; }

            public string codigo { get; set; }

            public int numeromodulo { get; set; }

            public virtual ICollection<EL.Cnf.usuariosrolesrel> usuariosrolesrel { get; set; }

            public virtual ICollection<EL.Cnf.usuariosrolesasegurablesrel> usuariosrolesasegurablesrel { get; set; }
        }

        public class usuariosrolesasegurablesrel
        {
            public Guid uidregist { get; set; }

            public Guid uidcia { get; set; }

            public Guid uidrole { get; set; }

            public Guid uidasegurable { get; set; }

            public int permisossumatoria { get; set; }

            public DateTime crefch { get; set; }

            public string creusr { get; set; }

            public string crehsn { get; set; }

            public string crehid { get; set; }

            public string creips { get; set; }

            public DateTime modfch { get; set; }

            public string modusr { get; set; }

            public string modhsn { get; set; }

            public string modhid { get; set; }

            public string modips { get; set; }

            public virtual usuariosasegurables usuariosasegurables { get; set; }

            public virtual usuariosroles usuariosroles { get; set; }
        }

        public class usuariosasegurables
        {
            public usuariosasegurables()
            {
                this.usuariosasegurables1 = (ICollection<usuariosasegurables>)new HashSet<usuariosasegurables>();
                this.usuariosrolesasegurablesrel = (ICollection<EL.Cnf.usuariosrolesasegurablesrel>)new HashSet<EL.Cnf.usuariosrolesasegurablesrel>();
            }

            public Guid uidregist { get; set; }

            public Guid uidcia { get; set; }

            public string codigo { get; set; }

            public short numerotipo { get; set; }

            public string descripci { get; set; }

            public int profundidad { get; set; }

            public Guid? uidregistpad { get; set; }

            public string pathcodigo { get; set; }

            public int permisossumatoria { get; set; }

            public DateTime crefch { get; set; }

            public string creusr { get; set; }

            public string crehsn { get; set; }

            public string crehid { get; set; }

            public string creips { get; set; }

            public DateTime modfch { get; set; }

            public string modusr { get; set; }

            public string modhsn { get; set; }

            public string modhid { get; set; }

            public string modips { get; set; }

            public int numeromodulo { get; set; }

            public bool? indauditoria { get; set; }

            public string entidadesauditadas { get; set; }

            public virtual ICollection<usuariosasegurables> usuariosasegurables1 { get; set; }

            public virtual usuariosasegurables usuariosasegurables2 { get; set; }

            public virtual ICollection<EL.Cnf.usuariosrolesasegurablesrel> usuariosrolesasegurablesrel { get; set; }
        }

        public class tblMenu
        {
            public Guid idMenu { get; set; }
            public string descripcion { get; set; }
            public string accion { get; set; }
            public Guid? idPadre { get; set; }
            public Guid idUsuarioCreacion { get; set; }
            public DateTime fechaCreacion { get; set; }
            public string ipCreacion { get; set; }
            public string hostCreacion { get; set; }
            public Guid? idUsuarioModificacion { get; set; }
            public DateTime? fechaModificacion { get; set; }
            public string ipModificacion { get; set; }
            public string hostModificacion { get; set; }
            public bool activo { get; set; }

        }

        public class tblMenuPermisos
        {
            public Guid idMenuPermisos { get; set; }
            public Guid idMenu { get; set; }
            public Guid idUsuario { get; set; }
            public Guid idRolUsuario { get; set; }
            public Guid idUsuarioCreacion { get; set; }
            public DateTime fechaCreacion { get; set; }
            public string ipCreacion { get; set; }
            public string hostCreacion { get; set; }
            public Guid? idUsuarioModificacion { get; set; }
            public DateTime? fechaModificacion { get; set; }
            public string ipModificacion { get; set; }
            public string hostModificacion { get; set; }
            public bool activo { get; set; }
        }

        public class tblRolUsuario
        {
            public Guid idRolUsuario { get; set; }
            public string codigo { get; set; }
            public string descricion { get; set; }
            public bool puedeVer { get; set; }
            public bool puedeCrear { get; set; }
            public bool puedeModificar { get; set; }
            public bool puedeBorrar { get; set; }
            public Guid idUsuarioCreacion { get; set; }
            public DateTime fechaCreacion { get; set; }
            public string ipCreacion { get; set; }
            public string hostCreacion { get; set; }
            public Guid? idUsuarioModificacion { get; set; }
            public DateTime fechaModificacion { get; set; }
            public string ipModificacion { get; set; }
            public string hostModificacion { get; set; }
            public bool activo { get; set; }
        }

        public class entidades
        {
            public Guid uidregist { get; set; }

            public Guid uidcia { get; set; }

            public int numeroentidadtipo { get; set; }

            public Int16 identificaciontipo { get; set; }

            public string numeroidentificacion { get; set; }

            public string nombreprimero { get; set; }

            public string nombresegundo { get; set; }

            public string apellidoprimero { get; set; }

            public string apellidosegundo { get; set; }

            public DateTime? fechanacimiento { get; set; }

            public Int16? numerogenero { get; set; }

            public string razonsocial { get; set; }

            public string nombrecomercial { get; set; }

            public string telefonomovil { get; set; }

            public string telefonomovilalterno { get; set; }

            public string correoelectronicoprimero { get; set; }

            public string correoelectronicosegundo { get; set; }

            public string correoelectronicotercero { get; set; }

            public string sitioweb { get; set; }

            public int numeroestado { get; set; }

            public int numeroobjeto { get; set; }

            public string escriturapublicalibro { get; set; }

            public int? escriturapublicanumero { get; set; }

            public string escriturapublicatomo { get; set; }

            public string escriturapublicafolio { get; set; }

            public string escriturapublicaasiento { get; set; }

            public int? cantidadempleados { get; set; }

            public byte? indmesacambio { get; set; }
            public string clientecodigo { get; set; }
            public string proveedorcodigo { get; set; }
            public string empleadocodigo { get; set; }
            public string beneficiariocodigo { get; set; }

            public DateTime crefch { get; set; }

            public string creusr { get; set; }

            public string crehsn { get; set; }

            public string crehid { get; set; }

            public string creips { get; set; }

            public DateTime modfch { get; set; }

            public string modusr { get; set; }

            public string modhsn { get; set; }

            public string modhid { get; set; }

            public string modips { get; set; }

        }

        public class periodofiscal
        {
            public Guid uidregist { get; set; }

            public Guid uidcia { get; set; }

            public string descripci { get; set; }

            public int numeroobjeto { get; set; }

            public int numeroestado { get; set; }

            public DateTime fechainicial { get; set; }

            public DateTime fechafinal { get; set; }

            public short yearfiscal { get; set; }

            public Guid? uidasientocierremonbas { get; set; }

            public Guid? uidasientocierremonfor { get; set; }

            public Guid? uidasientocierremonxtr { get; set; }

            public DateTime crefch { get; set; }

            public string creusr { get; set; }

            public string crehsn { get; set; }

            public string crehid { get; set; }

            public string creips { get; set; }

            public DateTime modfch { get; set; }

            public string modusr { get; set; }

            public string modhsn { get; set; }

            public string modhid { get; set; }

            public string modips { get; set; }

            public byte periodos { get; set; }

            public bool indcierreanualmonbas { get; set; }

            public bool indcierreanualmonfor { get; set; }

            public bool indcierreanualmonxtr { get; set; }
        }

        #endregion

        #region PROCEDIMIENTOS ALMACENADOS

        public class spgetUsuarios
        {
            public spgetUsuarios() { }

            public int idUsuario { set; get; }
            public int idCompany { set; get; }
            public string uidregist { set; get; }
            public string uidcia { set; get; }
            [Required(ErrorMessage = "El campo de usuario es requerido")]
            public string codigo { set; get; }
            public string codigoopesis { set; get; }
            public string descripci { set; get; }
            public short numerotipaut { set; get; }
            [Required(ErrorMessage = "El campo de password es requerido")]
            public string password { set; get; }
            public int numeroobjeto { set; get; }
            public int numeroestado { set; get; }
            public string imagen { set; get; }
            public short yearfiscalusing { set; get; }
            public short mesfiscalusin { set; get; }
            public short lenguajeusing { set; get; }
            public string uidempleado { set; get; }
            public string uidtelevendedor { set; get; }
            public string uidvendedor { set; get; }
            public string uidcobrador { set; get; }
            public string uidcajero { set; get; }
            public DateTime crefch { set; get; }
            public string creusr { set; get; }
            public string crehsn { set; get; }
            public string crehid { set; get; }
            public string creips { set; get; }
            public DateTime modfch { set; get; }
            public string modusr { set; get; }
            public string modhsn { set; get; }
            public string modhid { set; get; }
            public string modips { set; get; }

        }
        public class companias
        {

            public companias() { }
            public Guid uidregist { get; set; }
            public string numeroruc { get; set; }
            public string nombrecomercial { get; set; }
            public string razonsocial { get; set; }
            public string separadornivel { get; set; }
            public string telefonoprimero { get; set; }
            public string telefonosegundo { get; set; }
            public string telefonotercero { get; set; }
            public string faxprimero { get; set; }
            public string faxsegundo { get; set; }
            public string faxtercero { get; set; }
            public string telefonoexpresionregular { get; set; }
            public string telefonoexpresionformateo { get; set; }
            public string telefonoexpresionmascara { get; set; }
            public string direccioncompany { get; set; }
            public string sitioweb { get; set; }
            public string numerocedulaexpresionregular { get; set; }
            public string numerocedulaexpresionformateo { get; set; }
            public string numerocedulaexpresionmascara { get; set; }
            public string numerocedularesidenteexpresionregular { get; set; }
            public string numerocedularesidenteexpresionformateo { get; set; }
            public string numerocedularesidenteexpresionmascara { get; set; }
            public string numerocedulaprefijotemporal { get; set; }
            public string numerocedulasufijotemporal { get; set; }
            public string numerorucexpresionregular { get; set; }
            public string numerorucexpresionformateo { get; set; }
            public string numerorucexpresionmascara { get; set; }
            public string numerorucprefijotemporal { get; set; }
            public string correoelectronotificacion { get; set; }
            public string correoelectronotificacionpassword { get; set; }
            public string smtpservidor { get; set; }
            public Int16 smtppuerto { get; set; }
            public decimal tipocambiofactoranual { get; set; }
            public string carpetaayudasistema { get; set; }
            public Int16 departamentosegmentos { get; set; }
            public Int16 departamentodigitos { get; set; }
            public Int16 departamentodigitossegmento01 { get; set; }
            public Int16 departamentodigitossegmento02 { get; set; }
            public Int16 departamentodigitossegmento03 { get; set; }
            public Int16 departamentodigitossegmento04 { get; set; }
            public Int16 departamentodigitossegmento05 { get; set; }
            public string departamentopatronsegmento01 { get; set; }
            public string departamentopatronsegmento02 { get; set; }
            public string departamentopatronsegmento03 { get; set; }
            public string departamentopatronsegmento04 { get; set; }
            public string departamentopatronsegmento05 { get; set; }
            public string departamentoexpresionregular { get; set; }
            public string departamentoexpresionformateo { get; set; }
            public string departamentoexpresionmascara { get; set; }
            public string uidpais { get; set; }
            public string uidpaisdepto { get; set; }
            public string uidpaisdeptomunicipio { get; set; }
            public int idPais { get; set; }
            public int idDepartamento { get; set; }
            public int idMunicipio { get; set; }
            public byte imagentipostorage { get; set; }
            public string usuarioimagen { get; set; }
            public string usuariopasswordimagen { get; set; }
            public string imagenfolder { get; set; }
            public string imagenappkey { get; set; }
            public string imagenappsecret { get; set; }
            public string imagenaccesstoken { get; set; }
            public string imagensplash { get; set; }
            public string imagen { get; set; }
            public DateTime crefch { get; set; }
            public string creusr { get; set; }
            public string crehid { get; set; }
            public string creips { get; set; }
            public DateTime modfch { get; set; }
            public string modusr { get; set; }
            public string modhid { get; set; }
            public string modips { get; set; }
            public string licenciakey { get; set; }
            public bool indvalidarfechanacimientoencedula { get; set; }
            public bool indmostrarmasterdashboard { get; set; }

        }
        public class companiasView
        {
            public Guid idCompany { get; set; }
            public Guid idUsuario { get; set; }
            public string nombrecomercial { get; set; }
            public string razonsocial { get; set; }
            public string numeroruc { get; set; }
            public string telefonoprimero { get; set; }
            public string direccioncompany { get; set; }
            public string pais { get; set; }
        }
        public class spGetModulosPorUsuario
        {
            public string menu { get; set; }
            public int numerotipo { get; set; }
            public Guid uidregistpad { get; set; }
            public Guid uidregist { get; set; }
            public string permiso { get; set; }
        }
        public class spGetListUsuario
        {
            public Guid uidregist { get; set; }
            public string codigo { get; set; }
        }
        public class spGetNewMenuList
        {
            public Guid idMenu { get; set; }
            public Guid idPadre { get; set; }
            public string descripcion { get; set; }
            public string accion { get; set; }
            public string rol { get; set; }
        }
        public class spGetPermisosAsignados
        {
            public Guid idMenuPermisos { get; set; }
            public Guid idMenu { get; set; }
            public Guid idUsuario { get; set; }
            public Guid idRolUsuario { get; set; }
            public string Menus { get; set; }
            public string categoria { get; set; }
            public string usuario { get; set; }
            public string usuario_nombre { get; set; }
            public string permiso { get; set; }
            public DateTime fechaCreacion { get; set; }
            public DateTime? fechaModificacion { get; set; }
        }
        public class spGetPermisosAsignadosPorUsuario
        {
            public Guid idMenuPermisos { get; set; }
            public string descripcionMenu { get; set; }
            public string usuarioName { get; set; }
            public string rolName { get; set; }
        }
        public class spMenuToCbo
        {
            public Guid idMenu { get; set; }
            public Guid? idPadre { get; set; }
            public string descripcion { get; set; }
        }
        public class spModuloInfo
        {
            public string descripcion { get; set; }
            public string accion { get; set; }
            public string area { get; set; }
        }
        public class spGetDataToCbo
        {
            public Guid identificador { get; set; }
            public string descripcion { get; set; }
        }
        public class spTasaCambio
        {
            public decimal tasaoficial { get; set; }
            public decimal tasacompra { get; set; }
            public decimal tasaventa { get; set; }
            public DateTime fechatransa { get; set; }

        }
        public class spGetCatalogoPaisDeptoMunic
        {
            public Guid idRegistro { get; set; }
            public string nombre { get; set; }
        }
        public class spGetPeriodosPorCompany
        {
            public Guid uidregist { get; set; }
            public Guid uidcia { get; set; }
            public string descripci { get; set; }
            public int periodo { get; set; }
            public string estado { get; set; }
            public DateTime fechainicial { get; set; }
            public DateTime fechafinal { get; set; }
        }
        public class spGetPeriodoMeses
        {
            public Guid uidregist { get; set; }
            public Guid uidregistpad { get; set; }
            public int mescalendario { get; set; }
            public int yearcalendario { get; set; }
            public DateTime fechainicial { get; set; }
            public DateTime fechafinal { get; set; }
            public string estado { get; set; }
            public string mes { get; set; }
        }

        #endregion

        #region TREEVIEW MÓDULOS
        public class Node
        {
            public Node()
            {
                nodes = new List<Node>();
            }

            public Node(string text, Guid idMenu, Guid? idPadre, List<Node> nodes, NodeState state)
            {
                this.text = text;
                this.idMenu = idMenu;
                this.idPadre = idPadre;
                this.nodes = nodes;
                this.state = state;
            }

            public Node(string text, List<Node> nodes)
            {
                this.text = text;
                this.nodes = nodes;
            }

            public string text { get; set; }
            public Guid idMenu { get; set; }
            public Guid? idPadre { get; set; }
            public List<Node> nodes { get; set; }
            public NodeState state { get; set; }
        }

        public class NodeState
        {
            public NodeState() { }

            public NodeState(bool expanded, bool selected)
            {
                this.expanded = expanded;
                this.selected = selected;
            }

            public bool expanded { get; set; }
            public bool selected { get; set; }
        }
        #endregion

        #region PERIODO FISCAL BI
        public class spGetPeriodoFiscal
        {
            public Int16 yearfiscal { get; set; }
            public Int16 mesfiscal { get; set; }
            public string mescalendariodescripci { get; set; }
            //public int numeroestado { get; set; }
        }
        #endregion

        #region PERIODO FISCAL BI 2
        public class spGetPeriodoFiscalBi
        {
            public Int16 yearfiscal { get; set; }
            public Int16 mesfiscal { get; set; }
            public string mescalendariodescripci { get; set; }
            //public int numeroestado { get; set; }
        }
        #endregion

        #region MONEDA
        public class mpxmonedasgle
        {
            public Guid uidregist { get; set; }
            public Int16 numero { get; set; }
            public string codigo { get; set; }
            public string simbolo { get; set; }
            public string descripci { get; set; }

        }
        #endregion


        #region MULTIPLE INFORMACION PARA UN COMBO
        public class spGetMultipleInfoCbo
        {
            public Guid id { get; set; }
            public string descripcion { get; set; }

            public string texto1 { get; set; }
            public string texto2 { get; set; }
            public string texto3 { get; set; }

            public int int1 { get; set; }
            public int int2 { get; set; }
            public int int3 { get; set; }

            public decimal decimal1 { get; set; }
            public decimal decimal2 { get; set; }
            public decimal decimal3 { get; set; }

            public bool? bool1 { get; set; }
            public bool? bool2 { get; set; }
            public bool? bool3 { get; set; }

        }
        #endregion

    }
}
