﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EL;
using BL;

namespace SgpWebApi.Areas.Login.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/Login")]
    public class LoginController : ApiController
    {

        [HttpPost]
        [Route("verificarCredenciales")]
        public Reply verificarCredenciales(Models.Credenciales c)
        {
            Reply reply = new Reply();
            try
            {                
                Cnf.usuarios u = new Cnf.usuarios();
                u = new MetodoConf().getUsuario(c.usuario);
                if(u.uidregist.ToString() != "")
                {
                    if (new BL.sGpSecurity.LicenseValidation().acEncrypt(c.password) == u.password)
                    {
                        reply.mensaje = "Datos correctos";
                        reply.resultado = 1;
                        reply.data = u;
                    }
                    else
                    {
                        reply.mensaje = "Datos incorrectos";
                        reply.resultado = 0;
                    }
                }
                else
                {
                    reply.mensaje = "Datos incorrectos, usuario no existe";
                    reply.resultado = 0;
                }
                return reply;
            }catch(Exception e)
            {
                reply.mensaje = "Ocurrió un error al procesar los datos";
                reply.resultado = 0;
                return reply;
                throw e;
            }
        }


        [HttpGet]
        [Route("verificarCredencialesGET")]
        public Reply verificarCredencialesGET(string usuario, string password)
        {
            Reply reply = new Reply();
            try
            {
                Cnf.usuarios u = new Cnf.usuarios();
                u = new MetodoConf().getUsuario(usuario);
                if (u.uidregist.ToString() != "")
                {
                    if (new BL.sGpSecurity.LicenseValidation().acEncrypt(password) == u.password)
                    {
                        reply.mensaje = "Datos correctos";
                        reply.resultado = 1;
                        reply.data = u;
                    }
                    else
                    {
                        reply.mensaje = "Datos incorrectos";
                        reply.resultado = 0;
                    }
                }
                else
                {
                    reply.mensaje = "Datos incorrectos, usuario no existe";
                    reply.resultado = 0;
                }
                return reply;
            }
            catch (Exception e)
            {
                reply.mensaje = "Ocurrió un error al procesar los datos";
                reply.resultado = 0;
                return reply;
                throw e;
            }
        }
    }
}
