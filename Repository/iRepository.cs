﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Repository
{
    public interface iRepository : IDisposable
    {
        TEntity create<TEntity>(TEntity obj) where TEntity : class;
        bool update<TEntity>(TEntity obj) where TEntity : class;
        bool delete<TEntity>(TEntity obj) where TEntity : class;
        TEntity findEntity<TEntity>(Expression<Func<TEntity, bool>> criterio) where TEntity : class;
        List<TEntity> listEntity<TEntity>(Expression<Func<TEntity, bool>> criterio) where TEntity : class;
    }
}
