﻿using LogicNP.CryptoLicensing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.sGpSecurity
{
    public class LicenseValidation
    {
        private string acLicenseCode = string.Empty;
        public const string validationKey = "AMAAMACzzBrO7YMKVidYG1Cf3/9tNdPS7RXa+P8EMsolILOBQO5C684zo/RgPOt/HKlpT4cDAAEAAQ==";

        public LicenseValidation() { }

        public LicenseValidation(string pcLicense)
        {
            this.acLicenseCode = pcLicense;
        }

        public CryptoLicense License { get; set; }

        public bool Validate()
        {
            try
            {
                this.License = new CryptoLicense(this.acLicenseCode, "AMAAMACzzBrO7YMKVidYG1Cf3/9tNdPS7RXa+P8EMsolILOBQO5C684zo/RgPOt/HKlpT4cDAAEAAQ==");
                return this.License.Status == LicenseStatus.Valid;
            }
            catch (Exception ex)
            {
                ex.Data.Add((object)"License", (object)"Validacion de Licencia fallida!!!");
                return false;
            }
        }

        public string acEncrypt(string pcPassword)
        {
            this.License = new CryptoLicense("AMAAMACzzBrO7YMKVidYG1Cf3/9tNdPS7RXa+P8EMsolILOBQO5C684zo/RgPOt/HKlpT4cDAAEAAQ==");
            string empty = string.Empty;
            return this.License.EncryptString(pcPassword);
        }
        public string acDecrypt(string pcPassword)
        {
            this.License = new CryptoLicense("AMAAMACzzBrO7YMKVidYG1Cf3/9tNdPS7RXa+P8EMsolILOBQO5C684zo/RgPOt/HKlpT4cDAAEAAQ==");
            string empty = string.Empty;
            return this.License.DecryptString(pcPassword);
        }
    }
}
