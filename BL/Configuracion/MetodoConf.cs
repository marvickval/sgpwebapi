﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EL;
using DAL;
using System.Data;
using System.Web.Script.Serialization;

namespace BL
{
    public class MetodoConf
    {

        #region USUARIOS

        /// <summary>
        /// METODO QUE RETORNA UN USUARIO
        /// </summary>
        /// <param name="usuario">USUARIO</param>
        /// <returns></returns>
        public Cnf.usuarios getUsuario(string usuario)
        {
            try
            {
                return ExecuteConnection.FindUser(new Cnf.usuarios(), usuario);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public List<Cnf.spgetUsuarios> getListUsuario(string usuario)
        {
            try
            {
                return ExecuteConnection.SpToObj(new Cnf.spgetUsuarios(), usuario);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE RETORNA UNA LISTA DE USUARIOS
        /// </summary>
        /// <returns></returns>
        public List<Cnf.usuarios> getListUsuarios()
        {
            try
            {
                return ExecuteConnection.DbToObj(new Cnf.usuarios(), 2, null, "descripci", 1);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE ACTUALIZA LA INFORMACION DE UN USUARIO
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public bool updateUsuario(Cnf.usuarios usuario)
        {
            try
            {
                return ExecuteConnection.ObjUpdate(usuario);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE ACTUALIZA LA INFORMACION DE UN USUARIO
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public Dictionary<string, object> updateUsuario(Dictionary<string, object> dictionary)
        {
            try
            {
                return (Dictionary<string, object>)ExecuteConnection.execSP("cnf.spActualizarPeriodoFiscalUsuario", dictionary, true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region MODULOS

        /// <summary>
        /// METODO QUE RETONAR UNA LISTA CON LOS MENUS QUE EL USUARIO TIENE ACCESO
        /// </summary> 
        /// <param name="uidUsuario"></param>
        /// <returns></returns>
        public List<Cnf.spGetNewMenuList> getMenuList(string uidUsuario, string uidCompany = null, Int16 intMesFiscal = 0)
        {
            //return ExecuteConnection.SpToObj(new Cnf.spGetModulosPorUsuario(), "'" + uidUsuario + "', '" + uidCompany + "'");
            return ExecuteConnection.SpToObj(new Cnf.spGetNewMenuList(), "'" + uidUsuario + "'," + intMesFiscal);
        }

        /// <summary>
        /// METODO QUE RETONAR UNA LISTA CON LOS MENUS QUE EL USUARIO TIENE ACCESO
        /// </summary> 
        /// <param name="uidUsuario"></param>
        /// <returns></returns>
        public List<Cnf.spGetPermisosAsignadosPorUsuario> getMenuListPermisosAsignadosPorUsuario(string uidUsuario)
        {
            return ExecuteConnection.SpToObj(new Cnf.spGetPermisosAsignadosPorUsuario(), "'" + uidUsuario + "'");
        }

        /// <summary>
        /// METODO QUE RETONAR UNA LISTA DE LA TASA DE CAMBIO
        /// </summary> 
        /// <param name="fecha"></param>
        /// <param name="proceso"></param>
        /// <returns></returns>
        public List<Cnf.spTasaCambio> getTasaCambio(string fecha, int proceso)
        {
            return ExecuteConnection.SpToObj(new Cnf.spTasaCambio(), "'" + fecha + "','" + proceso + "'");
        }

        /// <summary>
        /// METODO QUE RETORNA LA LISTA DE TODOS LOS MENUS
        /// </summary>
        /// <returns></returns>
        public List<Cnf.tblMenu> getAllMenu()
        {
            return ExecuteConnection.DbToObj(new Cnf.tblMenu(), 2, null, "idpadre", 1);
        }

        /// <summary>
        /// RETORNA UNA LISTA DE MENU FORMATEADOS PARA COMBOBOX
        /// </summary>
        /// <returns></returns>
        public List<Cnf.spMenuToCbo> getMenuToCbo()
        {
            try
            {
                return ExecuteConnection.SpToObj(new Cnf.spMenuToCbo());
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE GUARDAR UN MENU
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public bool guardarMenu(Cnf.tblMenu menu)
        {
            try
            {
                return ExecuteConnection.ObjAdd(menu);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE GUARDAR UN MENU
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public Dictionary<string, object> guardarMenu(Dictionary<string, object> dictionary)
        {
            try
            {
                return (Dictionary<string, object>)ExecuteConnection.execSP("cnf.spGuardarMenu", dictionary, true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE ACTUALIZA UN MENU
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public bool actualizarMenu(Cnf.tblMenu menu)
        {
            try
            {
                return ExecuteConnection.ObjUpdate(menu);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE RETORNA UN MENU PARA SU MODIFICACIÓN
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Cnf.tblMenu recuperarMenu(Guid id)
        {
            return ExecuteConnection.ObjFind(new Cnf.tblMenu(), id);
        }

        /// <summary>
        /// OBTIENE LA INFORMACION DEL MENU SELECCIONADO PARA LA CREACION DE AREAS Y DEMAS EN EL SISTEMA
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Cnf.spModuloInfo recuperarMenuInfo(Guid? id)
        {
            try
            {
                Cnf.spModuloInfo info = new Cnf.spModuloInfo();
                List<Cnf.spModuloInfo> list = ExecuteConnection.SpToObj(new Cnf.spModuloInfo(), "" + "'" + id + "'");
                info.accion = list[0].accion;
                info.area = list[0].area;
                info.descripcion = list[0].descripcion;
                return info;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE RETORNA LA LISTA DE MENUS EN FORMATO JSON
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public string getTreeData(List<Cnf.tblMenu> list, Guid? idMenuToEdit)
        {
            Cnf.Node nodePadre = new Cnf.Node("SELECCIONE MENU PADRE", new List<Cnf.Node>());
            try
            {
                foreach (Cnf.tblMenu item in list.Where(i => i.idPadre == Guid.Empty).OrderBy(i => i.descripcion).ToList())
                {
                    Cnf.Node nodeChild;
                    Cnf.NodeState state = new Cnf.NodeState();
                    if (idMenuToEdit != null)
                    {
                        if (item.idPadre == Guid.Empty)
                        {
                            if (item.idMenu == idMenuToEdit)
                            {
                                state.expanded = true;
                                state.selected = true;
                            }
                            else
                            {
                                state.expanded = false;
                                state.selected = false;
                            }
                            nodeChild = new Cnf.Node(item.descripcion, item.idMenu, item.idPadre, new List<Cnf.Node>(), state);
                            nodePadre.nodes.Add(nodeChild);
                            agregarChildren(list, nodeChild, item.idMenu, idMenuToEdit);
                        }
                    }
                    else
                    {
                        nodeChild = new Cnf.Node(item.descripcion, item.idMenu, item.idPadre, new List<Cnf.Node>(), state);
                        nodePadre.nodes.Add(nodeChild);
                        agregarChildren(list, nodeChild, item.idMenu, idMenuToEdit);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return (new JavaScriptSerializer().Serialize(nodePadre).Replace(",\"nodes\":[]", " "));
        }

        /// <summary>
        /// METODO QUE AGREGA ITEMS DE MENUS A UN DETERMINADO MENU (PADRE)
        /// </summary>
        /// <param name="list"></param>
        /// <param name="nodePadre"></param>
        /// <param name="idPadre"></param>
        public bool agregarChildren(List<Cnf.tblMenu> list, Cnf.Node nodePadre, Guid idPadre, Guid? idMenuToEdit)
        {
            var listItems = list.Where(i => i.idPadre == idPadre).OrderBy(i => i.descripcion).ToList();
            try
            {
                foreach (Cnf.tblMenu item in listItems)
                {
                    Cnf.Node nodeChild;
                    Cnf.NodeState state = new Cnf.NodeState();
                    if (idMenuToEdit != null)
                    {
                        if (item.idMenu == idMenuToEdit)
                        {
                            state.expanded = true;
                            state.selected = true;
                        }
                        else
                        {
                            state.expanded = false;
                            state.selected = false;
                        }
                        nodeChild = new Cnf.Node(item.descripcion, item.idMenu, item.idPadre, new List<Cnf.Node>(), state);
                        nodePadre.nodes.Add(nodeChild);
                        agregarChildren(list, nodeChild, item.idMenu, idMenuToEdit);
                    }
                    else
                    {
                        nodeChild = new Cnf.Node(item.descripcion, item.idMenu, item.idPadre, new List<Cnf.Node>(), state);
                        nodePadre.nodes.Add(nodeChild);
                        agregarChildren(list, nodeChild, item.idMenu, idMenuToEdit);
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            if (listItems.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region ASINGAR PERMISOS

        public List<Cnf.spGetPermisosAsignados> getPermisosAsignados()
        {
            try
            {
                return ExecuteConnection.SpToObj(new Cnf.spGetPermisosAsignados());
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE RETORNA UNA LISTA DE ROLES
        /// </summary>
        /// <returns></returns>
        public List<Cnf.tblRolUsuario> getListRoles()
        {
            try
            {
                return ExecuteConnection.DbToObj(new Cnf.tblRolUsuario(), 2, null, "descripcion", 1);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE GUARDA UNA NUEVA ASIGNACION DE PERMISOS
        /// </summary>
        /// <param name="tblMenuPermisos"></param>
        /// <returns></returns>
        public bool guardarAsignacion(Cnf.tblMenuPermisos tblMenuPermisos)
        {
            try
            {
                return ExecuteConnection.ObjAdd(tblMenuPermisos);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE GUARDA UNA NUEVA ASIGNACION DE PERMISOS DESDE UN SP
        /// </summary>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        public Dictionary<string, object> guardarAsignacion(Dictionary<string, object> dictionary)
        {
            try
            {
                return (Dictionary<string, object>)ExecuteConnection.execSpDataArray("cnf.spGuardarPermisos", dictionary, true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE ELIMINA UN PERMISO DE LA BASE DE DATOS
        /// </summary>
        /// <param name="idMenuPermiso"></param>
        /// <returns></returns>
        public Dictionary<string, object> eliminarAsignacion(Dictionary<string, object> paramentro)
        {
            try
            {
                //return ExecuteConnection.ExecuteSql("delete from cnf.tblMenuPermisos where idMenuPermisos = '" + idMenuPermiso + "'");
                //return ExecuteConnection.ExecuteSql("exec cnf.spEliminarPermisos '" + idMenuPermiso + "'");
                return (Dictionary<string, object>)ExecuteConnection.execSP("cnf.spEliminarPermisos", paramentro, false);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region PERIODO FISCAL
        /// <summary>
        /// MÉTODO QUE RETORNA LA LISTA DE PERIODOS POR EMRPESA
        /// </summary>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        public List<Cnf.spGetPeriodosPorCompany> getListPeriodos(Guid idEmpresa)
        {
            try
            {
                return ExecuteConnection.SpToObj(new Cnf.spGetPeriodosPorCompany(), "'" + idEmpresa + "'");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// METODO QUE RETORNA UNA LISTA DE MESES QUE PERTENECEN AL PERIODO SELECCIONADO
        /// </summary>
        /// <param name="idPeriodo"></param>
        /// <returns></returns>
        public List<Cnf.spGetPeriodoMeses> getListPeriodosMeses(Guid idPeriodo)
        {
            try
            {
                return ExecuteConnection.SpToObj(new Cnf.spGetPeriodoMeses(), "'" + idPeriodo + "'");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string getPeriodoEstado(int mesFiscal, int anioFiscal)
        {
            try
            {
                return "" + ExecuteConnection.SingleData("select iif(numeroestado = 256, 'Cerrado', 'Activo') 'estado' from cnf.periodofiscaldetalle where mesfiscal = " + mesFiscal + " and yearfiscal = " + anioFiscal);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region LLENADO DE CUALQUIER COMBO

        /// <summary>
        /// METODO QUE LLENA UN COMBOBOX
        /// </summary>
        /// <param name="nombreEntidadDB">          NOMBRE DE LA ENTIDAD EJ.: 'inv.articulos'</param>
        /// <param name="nombreIdentificadorBD">    NOMBRE DE LA COLUMNA ID</param>
        /// <param name="nombreDescripcionDB">      NOMBRE DE LA COLUMNA DESCRIPCION</param>
        /// <param name="companyID">                ID DE LA COMPAÑIA</param>
        /// <param name="filtrarRegistPad">              SI ES 1 TRAERA TODOS LOS REGISTROS DONDE uidregistpad SEA NULL</param>
        /// <returns></returns>
        public List<Cnf.spGetDataToCbo> llenarCbo(string nombreEntidadDB, string nombreIdentificadorBD, string nombreDescripcionDB, string nombreIdCompanyDB, Guid? companyID, int filtrarRegistPad)
        {
            try
            {
                if (companyID != null)
                {
                    return ExecuteConnection.SpToObj(new Cnf.spGetDataToCbo(), "'" + nombreEntidadDB + "'" + "," + "'" + nombreIdentificadorBD + "'" + "," + "'" + nombreDescripcionDB + "'" + "," + "'" + nombreIdCompanyDB + "'" + "," + "'" + companyID + "', " + filtrarRegistPad);
                }
                else
                {
                    return ExecuteConnection.SpToObj(new Cnf.spGetDataToCbo(), "'" + nombreEntidadDB + "'" + "," + "'" + nombreIdentificadorBD + "'" + "," + "'" + nombreDescripcionDB + "'" + "," + nombreIdCompanyDB + "'" + "'" + "," + "'00000000-0000-0000-0000-000000000000', " + filtrarRegistPad);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region DATOS DEL PAIS, DEPARTAMENTO, MUNICIPIO

        /// <summary>
        /// METODO QUE RETORNA UNA LISTA DE CATALOGO SEGUN EL TIPO
        /// </summary>
        /// <param name="idPadre"></param>
        /// <param name="isDepto"></param>
        /// <returns></returns>
        public List<Cnf.spGetCatalogoPaisDeptoMunic> getCatalogo(Guid idPadre, int isDepto)
        {
            try
            {
                return ExecuteConnection.SpToObj(new Cnf.spGetCatalogoPaisDeptoMunic(), "'" + idPadre + "'," + isDepto);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        #endregion

        #region METODO QUE TE RETORNA MULTIPLE VALORES POR PROCESO
        /// <summary>
        /// METODO QUE RETORNA MULTIPLE VALORES PARA UN CBO DEPENDIENDO DE SU PROCESO
        /// </summary>
        /// <param name="idcompania"></param>
        /// <returns></returns>
        public List<Cnf.spGetMultipleInfoCbo> spGetMultipleInfoCbo(object idCompania, object idreferencia, object proceso)
        {
            try
            {
                return ExecuteConnection.SpToObj(new Cnf.spGetMultipleInfoCbo(), "'" + idCompania + "','" + idreferencia + "','" + proceso + "'");
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }
}
