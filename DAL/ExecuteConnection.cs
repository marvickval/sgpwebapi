﻿using System;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;

namespace DAL
{
    //[DebuggerNonUserCode]
    public class ExecuteConnection
    {

        private static SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["SGPDB-CONEXION"].ConnectionString);
        private static SqlCommand cmd = new SqlCommand("", cnx);

        #region METODOS CRUD
        /// <summary>
        /// INSERTA UN NUEVO ITEM EN LA TABLA REPRESENTADA CON LA INSTANCIA DE LA CLASE DE OBJETO
        /// </summary>
        /// <param name="objPoco">INSTANCIAS DE LA CLASE (PUEDE SER CUALQUIER TIPO DE OBJETO).</param>
        /// <remarks>INSERTA EL NUEVO REGISTRO DE LA INSTANCIA DE LA CLASE <paramref name="objPOco"/>.
        /// </remarks>
        public static bool ObjAdd<T>(T objPoco)
        {
            try
            {
                DataTable dt = new DataTable();
                Type typeobjpoco = typeof(T);
                string Sql = "";
                string insert = "";
                string tablename = GetSchema(typeobjpoco);
                string schema = tablename.Substring(0, tablename.IndexOf("."));
                if (GetExist(tablename) == false)
                {
                    dt = GetSql(String.Format("SELECT C.name as ColumnName, IIF(C.NAME = COL.NAME, 'true', 'false') is_identity, C.is_nullable FROM SYS.COLUMNS AS C " +
                        "INNER JOIN sys.objects AS O on o.object_id = c.object_id " +
                        "INNER JOIN sys.schemas AS S on S.schema_id = O.schema_id " +
                        "INNER JOIN SYS.INDEXES PK ON O.OBJECT_ID = PK.OBJECT_ID AND PK.IS_PRIMARY_KEY = 1 " +
                        "INNER JOIN SYS.INDEX_COLUMNS IC ON IC.OBJECT_ID = PK.OBJECT_ID AND IC.INDEX_ID = PK.INDEX_ID " +
                        "INNER JOIN SYS.COLUMNS COL ON PK.OBJECT_ID = COL.OBJECT_ID AND COL.COLUMN_ID = IC.COLUMN_ID " +
                        "WHERE OBJECT_NAME(C.OBJECT_ID) = '{0}' and s.name = '{1}'", tablename.Substring(tablename.IndexOf(".") + 1), tablename.Substring(0, tablename.IndexOf("."))));
                    SerializeDt(dt, tablename);
                }
                else
                {
                    dt = DeSerializeDt(tablename);
                }
                PropertyDescriptorCollection PropiedadesObjPoco = TypeDescriptor.GetProperties(typeobjpoco);

                foreach (PropertyDescriptor prop in PropiedadesObjPoco)
                {
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        if (dt.Rows[i][0].ToString().ToUpper() == prop.Name.ToUpper())
                        {
                            if (prop.GetValue(objPoco) == null && Convert.ToBoolean(dt.Rows[i][2]) == false)
                            {
                                break;
                            }
                            if (Convert.ToBoolean(dt.Rows[i][1]) == false)
                            {
                                if (prop.GetValue(objPoco) == null)
                                {
                                    Sql += "Null,";
                                    break;
                                }
                                else
                                {
                                    var p = prop.PropertyType.ToString();
                                    if (prop.PropertyType.ToString() == "System.String" || prop.PropertyType.ToString() == "System.Boolean" || prop.PropertyType.ToString() == "System.Guid")
                                    {
                                        Sql += "'" + prop.GetValue(objPoco).ToString() + "',";
                                        break;
                                    }
                                    else if (prop.PropertyType.ToString() == "System.Nullable`1[System.Guid]")
                                    {
                                        if (prop.GetValue(objPoco).ToString() == "00000000-0000-0000-0000-000000000000")
                                        {
                                            Sql += "Null,";
                                        }
                                        else
                                        {
                                            Sql += "'" + prop.GetValue(objPoco).ToString() + "',";

                                        }
                                        break;
                                    }
                                    else if ((prop.PropertyType.ToString() == "System.Nullable`1[System.Boolean]"))
                                    {
                                        if (prop.GetValue(objPoco).ToString() == "True")
                                        {
                                            Sql += 1 + ",";
                                        }
                                        else
                                        {
                                            Sql += 0 + ",";
                                        }
                                        break;
                                    }
                                    else if (prop.PropertyType.ToString() == "System.DateTime" || prop.PropertyType.ToString() == "System.Nullable`1[System.DateTime]")
                                    {
                                        string timeString = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffff");
                                        Sql += "CAST(N'" + timeString + "' AS DateTime2),";
                                        break;
                                    }
                                    else
                                    {
                                        Sql += prop.GetValue(objPoco).ToString() + ",";
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                //PRIMARY KEY
                                Sql += " NEWID(),";
                                break;
                            }
                        }
                    }
                }
                Sql = Sql.Remove(Sql.Length - 1);
                insert = "INSERT INTO " + tablename + " VALUES (" + Sql + ")";
                ExecuteSql(insert);
                return true;
            }
            catch (Exception e)
            {
                throw e;
                //return false;
            }

        }

        /**********************************************************************************************/
        /*******************************************FIN ADD********************************************/
        /**********************************************************************************************/

        /// <summary>
        /// ACTUALIZA UN REGISTRO DE LA TABLA REPRESENTADA CON LA INSTANCIAS DE LA CLASE DEL OBJETO BASADO EN LA LLAVE PRIMARIA 
        /// </summary>
        /// <param name="objPoco">INSTANCIAS DE LA CLASE (PUEDE SER CUALQUIER TIPO DE OBJETO).</param>
        /// <remarks>ACTUALIZA UN REGISTRO CON INFORMACION CARGADA EN objPOco <paramref name="objPOco"/>.
        /// </remarks>
        public static bool ObjUpdate<T>(T objPoco)
        {
            try
            {
                string Pk = "";
                string key = "";
                DataTable dt = new DataTable();
                Type typeobjpoco = typeof(T);
                string Sql = "";
                string Update = "";
                string tablename = GetSchema(typeobjpoco);
                if (GetExist(tablename) == false)
                {
                    dt = GetSql(String.Format("SELECT C.name as ColumnName, IIF(C.NAME = COL.NAME, 'true', 'false') is_identity, C.is_nullable, COL.NAME FROM SYS.COLUMNS AS C " +
                        "INNER JOIN sys.objects AS O on o.object_id = c.object_id " +
                        "INNER JOIN sys.schemas AS S on S.schema_id = O.schema_id " +
                        "INNER JOIN SYS.INDEXES PK ON O.OBJECT_ID = PK.OBJECT_ID AND PK.IS_PRIMARY_KEY = 1 " +
                        "INNER JOIN SYS.INDEX_COLUMNS IC ON IC.OBJECT_ID = PK.OBJECT_ID AND IC.INDEX_ID = PK.INDEX_ID " +
                        "INNER JOIN SYS.COLUMNS COL ON PK.OBJECT_ID = COL.OBJECT_ID AND COL.COLUMN_ID = IC.COLUMN_ID " +
                        "WHERE OBJECT_NAME(C.OBJECT_ID) = '{0}' and s.name = '{1}'", tablename.Substring(tablename.IndexOf(".") + 1), tablename.Substring(0, tablename.IndexOf("."))));
                    SerializeDt(dt, tablename);
                }
                else
                {
                    dt = DeSerializeDt(tablename);
                }
                PropertyDescriptorCollection PropiedadesObjPoco = TypeDescriptor.GetProperties(typeobjpoco);
                foreach (DataRow row in dt.Rows)
                {
                    if (row[1].ToString().ToUpper() == "1" || Convert.ToBoolean(row[1].ToString()))
                    {
                        Pk = row[0].ToString();
                        break;
                    }
                }
                if (Pk == "")
                {
                    return false;
                }
                Sql = "SET ";
                foreach (PropertyDescriptor prop in PropiedadesObjPoco)
                {
                    if (Pk.ToUpper() != prop.Name.ToUpper())
                    {
                        for (int i = 0; i <= dt.Rows.Count; i++)
                        {
                            if (prop.GetValue(objPoco) == null && Convert.ToBoolean(dt.Rows[i][2]) == false)
                            {
                                break;
                            }
                            if (Convert.ToBoolean(dt.Rows[i][1]) == false)
                            {
                                Sql += prop.Name.Replace("_", "") + " = ";
                                if (prop.GetValue(objPoco) == null)
                                {
                                    Sql += "Null,";
                                    break;
                                }
                                else
                                {
                                    if (prop.PropertyType.ToString() == "System.String" || prop.PropertyType.ToString() == "System.Boolean" || prop.PropertyType.ToString() == "System.Guid")
                                    {
                                        Sql += "'" + prop.GetValue(objPoco).ToString() + "',";
                                        break;
                                    }
                                    else if (prop.PropertyType.ToString() == "System.Nullable`1[System.Guid]")
                                    {
                                        if (prop.GetValue(objPoco).ToString() == "00000000-0000-0000-0000-000000000000")
                                        {
                                            Sql += "Null,";
                                        }
                                        else
                                        {
                                            Sql += "'" + prop.GetValue(objPoco).ToString() + "',";

                                        }
                                        break;
                                    }
                                    else if ((prop.PropertyType.ToString() == "System.Nullable`1[System.Boolean]"))
                                    {
                                        if (prop.GetValue(objPoco).ToString() == "True")
                                        {
                                            Sql += 1 + ",";
                                        }
                                        else
                                        {
                                            Sql += 0 + ",";
                                        }
                                        break;
                                    }
                                    else if (prop.PropertyType.ToString() == "System.DateTime" || prop.PropertyType.ToString() == "System.Nullable`1[System.DateTime]")
                                    {
                                        string timeString = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffff");
                                        Sql += "CAST(N'" + timeString + "' AS DateTime2),";
                                        break;
                                    }
                                    else if (prop.PropertyType.ToString() == "System.Decimal")
                                    {
                                        string data = prop.GetValue(objPoco).ToString();
                                        data = data.Replace(',', '.');
                                        Sql += data + ",";
                                        break;
                                    }
                                    else
                                    {
                                        Sql += prop.GetValue(objPoco).ToString() + ",";
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (prop.PropertyType.ToString() == "System.String" || prop.PropertyType.ToString() == "System.DateTime" || prop.PropertyType.ToString() == "System.Boolean" || prop.PropertyType.ToString() == "System.Nullable`1[System.DateTime]")
                        {
                            key = "'" + prop.GetValue(objPoco).ToString() + "'";
                        }
                        else
                        {
                            key = "'" + prop.GetValue(objPoco).ToString() + "'";
                        }
                    }
                }
                Sql = Sql.Remove(Sql.Length - 1);
                Update = "UPDATE " + tablename + " " + Sql + " WHERE " + Pk + " = " + key;
                ExecuteSql(Update);
                return true;
            }
            catch (Exception e)
            {
                throw e;
                //return false;
            }

        }

        /**********************************************************************************************/
        /*******************************************FIN UPDATE*****************************************/
        /**********************************************************************************************/

        /// <summary>
        /// RETORNA UN OBJETO DEACUERDO A SU LLAVE PRIMARIA
        /// </summary>
        /// <param name="objPoco">TIPO DE OBJETO DE LA CLASE</param>
        /// <param name="Id">IDENTIFICADOR A ENCONTRAR</param>
        /// <remarks>ENCUENTRA UN REGISTRO EN LA TABLA DEL TIPO DE objPOco <paramref name="objPOco"/>.
        /// </remarks>
        public static T ObjFind<T, T1>(T objPoco, T1 Id)
        {
            try
            {
                DataTable dt = new DataTable();
                string Pk = "";
                string key = "";
                string Sql = "";
                Type typeobjpoco = typeof(T);
                string tablename = GetSchema(typeobjpoco);
                if (GetExist(tablename) == false)
                {
                    dt = GetSql(String.Format("SELECT C.name as ColumnName, IIF(C.NAME = COL.NAME, 'true', 'false') is_identity, C.is_nullable FROM SYS.COLUMNS AS C " +
                        "INNER JOIN sys.objects AS O on o.object_id = c.object_id " +
                        "INNER JOIN sys.schemas AS S on S.schema_id = O.schema_id " +
                        "INNER JOIN SYS.INDEXES PK ON O.OBJECT_ID = PK.OBJECT_ID AND PK.IS_PRIMARY_KEY = 1 " +
                        "INNER JOIN SYS.INDEX_COLUMNS IC ON IC.OBJECT_ID = PK.OBJECT_ID AND IC.INDEX_ID = PK.INDEX_ID " +
                        "INNER JOIN SYS.COLUMNS COL ON PK.OBJECT_ID = COL.OBJECT_ID AND COL.COLUMN_ID = IC.COLUMN_ID " +
                        "WHERE OBJECT_NAME(C.OBJECT_ID) = '{0}' and s.name = '{1}'", tablename.Substring(tablename.IndexOf(".") + 1), tablename.Substring(0, tablename.IndexOf("."))));
                    //dt = GetSql("declare @tblInfo as table(ishidden bit,columnordinal int,name varchar(100),isnullable bit,systemtypeid int,systemtypename varchar(50)," +
                    //    "maxlength int,precision int,scale int,collationname varchar(200),usertypeid int,usertypedatabase varchar(200),usertypeschema varchar(200),usertypename varchar(200)," +
                    //    "assemblyqualifiedtypename varchar(200),xmlcollectionid int,xmlcollectiondatabase varchar(200),xmlcollectionschema varchar(200),xmlcollectionname varchar(200)," +
                    //    "isxmldocument bit,iscasesensitive bit,isfixed bit,sourceserver varchar(200),sourcedatabase varchar(200),sourceschema varchar(200),sourcetable varchar(200)," +
                    //    "sourcecolumn varchar(200),isidentity bit,ispartofuniquekey bit,isupdateable bit,iscomputedcolumn bit,issparsecolumn bit,ordinalorder varchar(200)," +
                    //    "orderbydescending varchar(200),orderbylist varchar(200),tdstypeid int, tdslength int,tdscollationid int,tdscollationsortid int) " +
                    //    "insert into @tblInfo exec sp_describe_first_result_set N'" + tablename.Substring(0, tablename.IndexOf(".")) + "." + tablename.Substring(tablename.IndexOf(".") + 1) +"' " +
                    //    "select name ColumnName, isidentity is_identity, isnullable is_nullable from @tblInfo");
                    SerializeDt(dt, tablename);
                }
                else
                {
                    dt = DeSerializeDt(tablename);
                }
                PropertyDescriptorCollection PropiedadesObjPoco = TypeDescriptor.GetProperties(typeobjpoco);
                foreach (DataRow row in dt.Rows)
                {
                    if (row[1].ToString().ToUpper() == "1" || Convert.ToBoolean(row[1].ToString()))
                    {
                        Pk = row[0].ToString();
                        break;
                    }
                }
                if (Pk == "")
                {
                    return default(T);
                }
                foreach (PropertyDescriptor prop in PropiedadesObjPoco)
                {
                    if (Pk.ToUpper() == prop.Name.ToUpper())
                    {

                        if (prop.PropertyType.ToString().ToUpper() == Id.GetType().ToString().ToUpper())
                        {
                            if (prop.PropertyType.ToString() == "System.String" || prop.PropertyType.ToString() == "System.DateTime" || prop.PropertyType.ToString() == "System.Boolean")
                            {
                                key = "'" + Id.ToString() + "'";
                            }
                            else
                            {
                                key = "'" + Id.ToString() + "'";
                            }
                        }
                        else
                        {
                            return default(T);
                        }
                    }
                }

                Sql = "SELECT * FROM " + tablename + " " + Sql + " WHERE " + Pk + " = " + key;
                DataTable dtt = new DataTable();
                dtt = GetSql(Sql);
                int cont = 0;
                var obj = Activator.CreateInstance(typeof(T));
                if (dtt.Rows.Count > 0)
                {
                    foreach (PropertyDescriptor prop in PropiedadesObjPoco)
                    {
                        if ((dtt.Rows[0][cont].GetType()).ToString() == "System.DBNull")
                        {
                            prop.SetValue(obj, null);
                        }
                        else
                        {
                            prop.SetValue(obj, dtt.Rows[0][cont]);
                        }
                        cont++;
                    }
                }
                return (T)Convert.ChangeType(obj, typeof(T));
            }
            catch (Exception e)
            {
                throw e;
                //return default(T);
            }
        }

        /**********************************************************************************************/
        /*******************************************FIN FIND*******************************************/
        /**********************************************************************************************/

        public static T FindUser<T, String>(T objPoco, String usuario)
        {
            try
            {
                string Sql = "";
                Type typeobjpoco = typeof(T);
                string tablename = GetSchema(typeobjpoco);
                PropertyDescriptorCollection PropiedadesObjPoco = TypeDescriptor.GetProperties(typeobjpoco);
                Sql = "SELECT * FROM " + tablename + " " + " WHERE codigo = '" + usuario + "'";
                DataTable dt = new DataTable();
                dt = GetSql(Sql);
                int cont = 0;
                var obj = Activator.CreateInstance(typeof(T));
                if (dt.Rows.Count > 0)
                {
                    foreach (PropertyDescriptor prop in PropiedadesObjPoco)
                    {
                        if ((dt.Rows[0][cont].GetType()).ToString() == "System.DBNull")
                        {
                            prop.SetValue(obj, null);
                        }
                        else
                        {
                            prop.SetValue(obj, dt.Rows[0][cont]);
                        }
                        cont++;
                    }
                }
                return (T)Convert.ChangeType(obj, typeof(T));
            }
            catch (Exception e)
            {
                throw e;
                //return default(T);
            }
        }

        /// <summary>
        /// DEVUELVE UNA LISTA TIPADA REALIZANDO UNA CONSULTA DE TODOS LOS CAMPOS A UNA TABLA
        /// </summary>
        /// <param name="objPoco">TIPO DE OBJETO DE LA CLASE</param> 
        /// <param name="Opt">PONER 0 PARA "SELECT * FROM A TABLE", PONER 1 PARA INDICAR UNA VISTA Y UNA CLAUSULA WHERE, PONER 2 PARA INDICAR "ORDER BY"</param>
        /// <param name="vw">SI Opt ES 1, INDICA EL NOMBRE DE LA VISTA
        /// <param name="Fields">SI Opt ES 1, INDICA LA COLUMNA Y EL VALOR QUE SERA FILTRADO, SE PUEDE FILTRAR POR TODAS LAS COLUMNAS QUE TENGA LA VISTA,
        ///  SI Opt ES 2, INDICA LA COLUMNA POR LA QUE SE QUIERE ORDENAR</param>
        ///  <param name="Sort">SI Opt ES 2 Y LA COLUMNA ES NULL Y Sort ES 0, ORDENA POR LA LLAVE PRIMARIA EN "DESC".
        ///  SI Opt ES 2 Y LA COLUMNA ES NULL Y Sort ES 1, ORDENA POR LA LLAVE PRIMARIA EN "ASC".</param>
        /// <remarks>
        /// </remarks>
        public static List<T> DbToObj<T>(T ObjPoco, int Opt = 0, string vw = null, string Fields = null, int Sort = 0)
        {
            try
            {
                DataTable dt = new DataTable();
                Type typeobjpoco = typeof(T);
                string tablename = GetSchema(typeobjpoco);
                string Pk = "";
                Pk = "" + SingleData(String.Format("SELECT COL_NAME(ic.OBJECT_ID,ic.column_id) AS ColumnName " +
                                              "FROM sys.indexes AS i " +
                                              "INNER JOIN sys.index_columns AS ic ON i.OBJECT_ID = ic.OBJECT_ID AND i.index_id = ic.index_id and i.is_primary_key = 1 " +
                                              "INNER JOIN sys.objects AS O on o.object_id = ic.object_id " +
                                              "INNER JOIN sys.schemas AS S on S.schema_id = O.schema_id " +
                                              "WHERE OBJECT_NAME(iC.OBJECT_ID) = '{0}' and s.name = '{1}'", tablename.Substring(tablename.IndexOf(".") + 1), tablename.Substring(0, tablename.IndexOf("."))));
                if (Opt == 2 && Pk != "" && Fields == null)
                {
                    Fields = Pk;
                }
                dt = GetQuery(tablename, Opt, vw, Fields, Sort);
                PropertyDescriptorCollection PropiedadesObjPoco = TypeDescriptor.GetProperties(typeobjpoco);
                var listobj = new List<T>();
                int cont = 0;
                for (int i = 0; i < (dt.Rows.Count); i++)
                {
                    var obj = Activator.CreateInstance(typeof(T));
                    foreach (PropertyDescriptor prop in PropiedadesObjPoco)
                    {
                        if ((dt.Rows[i][cont].GetType()).ToString() == "System.DBNull")
                        {
                            if (dt.Columns[cont].DataType.FullName.Equals("System.Guid"))
                            {
                                prop.SetValue(obj, Guid.Empty);
                            }
                            else if (dt.Columns[cont].DataType.FullName.Equals("System.DateTime") || dt.Columns[cont].DataType.FullName.Equals("System.Int32") || dt.Columns[cont].DataType.FullName.Equals("System.Int16") || dt.Columns[cont].DataType.FullName.Equals("System.Byte"))
                            {
                                prop.SetValue(obj, null);
                            }
                            else
                            {
                                prop.SetValue(obj, "");
                            }
                        }
                        else
                        {
                            prop.SetValue(obj, dt.Rows[i][cont]);
                            //if (dt.Rows[i][cont].GetType().ToString().Equals("System.DateTime"))
                            //{
                            //    DateTime date = Convert.ToDateTime(dt.Rows[i][cont]);
                            //    prop.SetValue(obj, date.ToString("yyyy-MM-ddTHH:mm:ss.fffffff"));
                            //}
                            //else
                            //{
                            //    prop.SetValue(obj, dt.Rows[i][cont]);
                            //}

                        }

                        cont++;
                    }
                    listobj.Add((T)Convert.ChangeType(obj, typeof(T)));
                    cont = 0;
                }
                return listobj;
            }
            catch (Exception e)
            {
                throw e;
                //return default(List<T>);
            }

        }

        /**********************************************************************************************/
        /*******************************************FIN DBTOOBJ****************************************/
        /**********************************************************************************************/

        /// <summary>
        /// DEVUELVE UNA LISTA TIPADA REALIZANDO UNA CONSULTA DE TODOS LOS CAMPOS A UNA TABLA
        /// </summary>
        /// <param name="objPoco">TIPO DE OBJETO DE LA CLASE</param> 
        /// <param name="Opt">PONER 0 PARA "SELECT * FROM A TABLE", PONER 1 PARA INDICAR UNA VISTA Y UNA CLAUSULA WHERE, PONER 2 PARA INDICAR "ORDER BY"</param>
        /// <param name="vw">SI Opt ES 0, INDICA EL NOMBRE DE LA VISTA
        /// <param name="Fields">SI Opt ES 1, INDICA LA COLUMNA Y EL VALOR QUE SERA FILTRADO, SE PUEDE FILTRAR POR TODAS LAS COLUMNAS QUE TENGA LA VISTA,
        ///  SI Opt ES 2, INDICA LA COLUMNA POR LA QUE SE QUIERE ORDENAR</param>
        ///  <param name="Sort">SI Opt ES 2 Y LA COLUMNA ES NULL Y Sort ES 0, ORDENA POR LA LLAVE PRIMARIA EN "DESC".
        ///  SI Opt ES 2 Y LA COLUMNA ES NULL Y Sort ES 1, ORDENA POR LA LLAVE PRIMARIA EN "ASC".</param>
        /// <remarks>
        /// </remarks>
        public static DataTable DbToDt<T>(T ObjPoco, int Opt = 0, string vw = null, string Fields = null, int Sort = 0)
        {
            try
            {
                DataTable dt = new DataTable();
                Type typeobjpoco = typeof(T);
                string tablename = GetSchema(typeobjpoco);
                string Pk = "";
                Pk = "" + SingleData(String.Format("SELECT COL_NAME(ic.OBJECT_ID,ic.column_id) AS ColumnName " +
                                              "FROM sys.indexes AS i " +
                                              "INNER JOIN sys.index_columns AS ic ON i.OBJECT_ID = ic.OBJECT_ID AND i.index_id = ic.index_id and i.is_primary_key = 1 " +
                                              "INNER JOIN sys.objects AS O on o.object_id = ic.object_id " +
                                              "INNER JOIN sys.schemas AS S on S.schema_id = O.schema_id " +
                                              "WHERE OBJECT_NAME(iC.OBJECT_ID) = '{0}' and s.name = '{1}'", tablename.Substring(tablename.IndexOf(".") + 1), tablename.Substring(0, tablename.IndexOf("."))));
                if (Opt == 2 && Pk != "" && Fields == null)
                {
                    Fields = Pk;
                }
                dt = GetQuery(tablename, Opt, vw, Fields, Sort);
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /**********************************************************************************************/
        /*******************************************FIN DBTODT*****************************************/
        /**********************************************************************************************/

        /// <summary>
        /// EJECUTA UN PROCEDIMIENTO ALMACENADO Y RETORNA SU RESULTADO EN UN DATATABLE
        /// </summary>
        /// <param name="objPoco">TIPO DE OBJETO DE LA CLASE</param>
        /// <remarks>
        /// </remarks>
        public static DataTable spToDataTable(string sp, string Param = null)
        {
            try
            {
                DataTable dt = new DataTable();
                if (Param != null)
                {
                    dt = GetSql("Exec " + sp + " " + Param);
                }
                else
                {
                    dt = GetSql("Exec " + sp);
                }
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// EJECUTA UN PROCEDIMIENTO ALMACENADO Y RETORNA SU RESULTADO EN UNA LISTA
        /// </summary>
        /// <param name="objPoco">TIPO DE OBJETO DE LA CLASE</param>
        /// <remarks>
        /// </remarks>
        public static List<T> SpToObj<T>(T objPoco, string Param = null)
        {
            try
            {
                DataTable dt = new DataTable();
                Type typeobjpoco = typeof(T);
                string tablename = GetSchema(typeobjpoco);
                if (Param != null)
                {
                    dt = GetSql("Exec " + tablename + " " + Param);
                }
                else
                {
                    dt = GetSql("Exec " + tablename);
                }

                PropertyDescriptorCollection PropiedadesObjPoco = TypeDescriptor.GetProperties(typeobjpoco);
                var listobj = new List<T>();
                int cont = 0;
                for (int i = 0; i < (dt.Rows.Count); i++)
                {
                    var obj = Activator.CreateInstance(typeof(T));
                    foreach (PropertyDescriptor prop in PropiedadesObjPoco)
                    {
                        if ((dt.Rows[i][cont].GetType()).ToString() == "System.DBNull")
                        {
                            if (dt.Columns[cont].DataType.FullName.Equals("System.Guid"))
                            {
                                prop.SetValue(obj, Guid.Empty);
                            }
                            else if (dt.Columns[cont].DataType.FullName.Equals("System.DateTime") || dt.Columns[cont].DataType.FullName.Equals("System.Int32") || dt.Columns[cont].DataType.FullName.Equals("System.Decimal") || dt.Columns[cont].DataType.FullName.Equals("System.Boolean"))
                            {
                                prop.SetValue(obj, null);
                            }
                            else
                            {
                                prop.SetValue(obj, "");
                            }
                        }
                        else
                        {
                            prop.SetValue(obj, dt.Rows[i][cont]);
                        }

                        cont++;
                    }
                    listobj.Add((T)Convert.ChangeType(obj, typeof(T)));
                    cont = 0;
                }
                return listobj;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// EJECUTA UN PROCEDIMIENTO ALMACENADO Y RETORNA UN OBJETO DEL TIPO QUE SE LE ENVIE
        /// </summary>
        /// <param name="objPoco">TIPO DE OBJETO DE LA CLASE</param>
        /// <remarks>
        /// </remarks>
        public static T SpToUniqueObj<T>(T objPoco, string Param = null)
        {
            try
            {
                DataTable dt = new DataTable();
                Type typeobjpoco = typeof(T);
                string tablename = GetSchema(typeobjpoco);
                if (Param != null)
                {
                    dt = GetSql("Exec " + tablename + " " + Param);
                }
                else
                {
                    dt = GetSql("Exec " + tablename);
                }

                PropertyDescriptorCollection PropiedadesObjPoco = TypeDescriptor.GetProperties(typeobjpoco);
                var listobj = new List<T>();
                int cont = 0;
                var obj = Activator.CreateInstance(typeof(T));
                for (int i = 0; i < (dt.Rows.Count); i++)
                {

                    foreach (PropertyDescriptor prop in PropiedadesObjPoco)
                    {
                        if ((dt.Rows[i][cont].GetType()).ToString() == "System.DBNull")
                        {
                            if (dt.Columns[cont].DataType.FullName.Equals("System.Guid"))
                            {
                                prop.SetValue(obj, Guid.Empty);
                            }
                            else if (dt.Columns[cont].DataType.FullName.Equals("System.DateTime") || dt.Columns[cont].DataType.FullName.Equals("System.Int32") || dt.Columns[cont].DataType.FullName.Equals("System.Decimal") || dt.Columns[cont].DataType.FullName.Equals("System.Boolean") || dt.Columns[cont].DataType.FullName.Equals("System.Byte[]"))
                            {
                                prop.SetValue(obj, null);
                            }
                            else
                            {
                                prop.SetValue(obj, "");
                            }
                        }
                        else
                        {
                            prop.SetValue(obj, dt.Rows[i][cont]);
                        }

                        cont++;
                    }
                    //listobj.Add((T)Convert.ChangeType(obj, typeof(T)));
                    cont = 0;
                }
                return (T)Convert.ChangeType(obj, typeof(T));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /**********************************************************************************************/
        /*******************************************FIN SPTOOBJ****************************************/
        /**********************************************************************************************/

        public static DataSet getMultipleData(string spName, string Param = null)
        {
            try
            {
                DataSet dsReturn = new DataSet();
                string tablename = spName;
                dsReturn = GetSqlWithMultipleTables("Exec " + tablename + " " + Param);
                return dsReturn;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// EJECUTA UN PROCEDIMIENTO ALMACENADO Y RETORNA SU RESULTADO EN UN DATATABLE
        /// </summary>
        /// <param name="objPoco">TIPO DE OBJETO DE LA CLASE</param>
        /// <remarks>
        /// </remarks>
        public static DataTable SpToDt<T>(T objPoco, string Param)
        {
            try
            {
                DataTable dt = new DataTable();
                Type typeobjpoco = typeof(T);
                string tablename = GetSchema(typeobjpoco);
                dt = GetSql("Exec " + tablename + " " + Param);
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /**********************************************************************************************/
        /*******************************************FIN SPTODT*****************************************/
        /**********************************************************************************************/

        /// <summary>
        /// EJECUTA UNA SENTENCIA SQL Y RETORNA SU RESULTADO EN UN DATATABLE
        /// </summary>
        /// <param name="Sql">SENTENCIA SQL</param>
        /// <remarks>
        /// </remarks>
        public static DataTable GetSql(string Sql)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter dta = new SqlDataAdapter();
            string query = Sql;
            try
            {
                dta.SelectCommand = new SqlCommand(query, cnx);
                cnx.Open();
                dt.Clear();
                dta.Fill(dt);
                cnx.Close();
            }
            catch (Exception e)
            {
                if (cnx.State == System.Data.ConnectionState.Open)
                {
                    cnx.Close();
                }

                throw e;

            }
            return dt;
        }

        /**********************************************************************************************/
        /*******************************************FIN GETSQL*****************************************/
        /**********************************************************************************************/

        /// <summary>
        /// EJECUTA UNA SENTENCIA SQL Y RETORNA SU RESULTADO EN UN DATATABLE
        /// </summary>
        /// <param name="Sql">SENTENCIA SQL</param>
        /// <remarks>
        /// </remarks>
        public static DataSet GetSqlWithMultipleTables(string Sql)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter dta = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string query = Sql;
            try
            {
                dta.SelectCommand = new SqlCommand(query, cnx);
                cnx.Open();
                dt.Clear();
                dta.Fill(ds);
                cnx.Close();
            }
            catch (Exception e)
            {
                if (cnx.State == System.Data.ConnectionState.Open)
                {
                    cnx.Close();
                }

                throw e;

            }
            return ds;
        }

        /**********************************************************************************************/
        /*******************************************FIN GETSQL*****************************************/
        /**********************************************************************************************/

        /// <summary>
        /// EJECUTA UNA SENTENCIA SQL O UN PROCEDIMIENTO ALMACENADO
        /// </summary>
        /// <param name="Sql">SENTENCIA SQL</param>
        /// <remarks>
        /// </remarks>

        public static bool ExecuteSql(string Sql)
        {
            try
            {
                cnx.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = Sql;
                cmd.ExecuteNonQuery();
                cnx.Close();
                return true;
            }
            catch (Exception e)
            {

                if (cnx.State == System.Data.ConnectionState.Open)
                {
                    cnx.Close();
                }
                throw e;
                //return false;
            }
        }

        /**********************************************************************************************/
        /*******************************************FIN EXCEUTESQL*************************************/
        /**********************************************************************************************/

        /// <summary>
        /// RETORNA EL ID DE UN OBJETO
        /// </summary>
        /// <typeparam name="T">PARAMETRO A BUSCAR</typeparam>
        /// <param name="objPoco">TIPO DE OBJETO DE LA CLASE</param>
        /// <param name="Opc">SI Opc ES 0 TOMA LA LLAVE PRIMARIA, SI Opc ES 1 BUSCA POR EL FIELD</param>
        /// <param name="Field">DATO A BUSCAR</param>
        /// <returns></returns>
        public static string GetId<T>(T objPoco, int Opc = 0, string Field = null)
        {
            try
            {
                string Pk = "";
                string Sql = "";
                Type typeobjpoco = typeof(T);
                string tablename = GetSchema(typeobjpoco);
                PropertyDescriptorCollection PropiedadesObjPoco = TypeDescriptor.GetProperties(typeobjpoco);
                Pk = "" + SingleData(String.Format("SELECT COL_NAME(ic.OBJECT_ID,ic.column_id) AS ColumnName " +
                                               "FROM sys.indexes AS i " +
                                               "INNER JOIN sys.index_columns AS ic ON i.OBJECT_ID = ic.OBJECT_ID AND i.index_id = ic.index_id and i.is_primary_key = 1 " +
                                               "INNER JOIN sys.objects AS O on o.object_id = ic.object_id " +
                                               "INNER JOIN sys.schemas AS S on S.schema_id = O.schema_id " +
                                               "WHERE OBJECT_NAME(iC.OBJECT_ID) = '{0}' and s.name = '{1}'", tablename.Substring(tablename.IndexOf(".") + 1), tablename.Substring(0, tablename.IndexOf("."))));
                if (Pk == "")
                {
                    return "";
                }
                if (Opc == 0)
                {
                    Sql = "SELECT TOP(1) " + Pk + " FROM " + tablename + " WHERE CONVERT(int, " + Pk + ") NOT LIKE '%[^0-9]%' ORDER BY CONVERT(int," + Pk + ") DESC";
                    return "" + SingleData(Sql);
                }
                else
                {
                    Sql = "SELECT TOP(1) " + Field + " FROM " + tablename + " WHERE CONVERT(int, " + Field + ") NOT LIKE '%[^0-9]%' ORDER BY CONVERT(int," + Field + ") DESC";
                    return "" + SingleData(Sql);
                }

            }
            catch (Exception e)
            {
                throw e;
                //return "";
            }



        }

        /**********************************************************************************************/
        /*******************************************FIN GETID******************************************/
        /**********************************************************************************************/

        /// <summary>
        /// VALIDA SI EL CAMPO EXISTE EN LA TABLA
        /// </summary>
        /// <param name="objPoco">OBJETO TIPADO</param>
        /// <param name="nombreCampo">CAMPO A VALIDAR</param>
        /// <remarks>
        /// </remarks>
        public static bool ValidarExiste<T>(T objeto, string nombreCampo)
        {
            bool resultado = false;
            string propiedad = "";
            string valor = "";
            string consulta = "";
            try
            {
                Type typeobjpoco = typeof(T);
                string tablename = GetSchema(typeobjpoco);
                PropertyDescriptorCollection PropiedadesObjPoco = TypeDescriptor.GetProperties(typeobjpoco);

                foreach (PropertyDescriptor prop in PropiedadesObjPoco)
                {
                    if (prop.Name.ToUpper() == nombreCampo.ToUpper().ToString())
                    {
                        //if (cont == indice)

                        propiedad = prop.Name.ToString();
                        valor = prop.GetValue(objeto).ToString();
                        break;
                    }
                }
                if (propiedad != "" || valor != "")
                {

                    consulta = string.Format(" Select * from " + tablename + " where " + propiedad + " = " + "'" + valor + "'");
                    resultado = (SingleData(consulta) != "" ? true : false);

                }
            }
            catch (Exception e)
            {

                throw e;
            }
            return resultado;
        }

        /**********************************************************************************************/
        /*******************************************FIN SINGLEDATA*************************************/
        /**********************************************************************************************/

        /// </summary>
        /// <param name="sp">NOMBRE DEL STORE PROCEDURE</param>
        /// <param name="param">PARAMETROS DEL STORE PROCEDURE</param>
        /// <param name="returnvalue">TRUE SI EL STORE PROCEDURE RETORNA X VALORES</param>
        /// <returns></returns>
        public static object execSP(string sp, Dictionary<string, object> param = null, bool returnvalue = false)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = GetSpInfo(sp);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = sp;
                cmd.Parameters.Clear();
        /// <summary>
        /// METODO QUE EJECUTA UN STORE PROCEDURE CON X CANTIDAD DE PARAMETROS Y RETORNA X CANTIDAD DE OUTPUT
                Dictionary<string, object> dictionaryReturned = returnvalue == true ? new Dictionary<string, object>() : null;
                if (param != null)
                {
                    if (dictionaryReturned != null)
                    {
                        dictionaryReturned.Clear();
                    }
                    foreach (KeyValuePair<string, object> keyobject in param)
                    {
                        string key = keyobject.Key.ToString();
                        object value = keyobject.Value == null ? null : keyobject.Value;
                        string type = keyobject.GetType().ToString();
                        foreach (DataRow row in dt.Rows)
                        {
                            if (row[0].ToString() == key)
                            {
                                if (key.Substring(key.Length - 3) == "OUT")
                                {
                                    SqlParameter ValorRetorno = null;
                                    switch (row[1].ToString())
                                    {
                                        case "int":
                                            ValorRetorno = new SqlParameter(key, SqlDbType.Int, Convert.ToInt32(row[2].ToString()));
                                            ValorRetorno.Direction = ParameterDirection.Output;
                                            break;
                                        case "nvarchar":
                                            ValorRetorno = new SqlParameter(key, SqlDbType.NVarChar, Convert.ToInt32(row[2].ToString()));
                                            ValorRetorno.Direction = ParameterDirection.Output;
                                            break;
                                        case "varchar":
                                            ValorRetorno = new SqlParameter(key, SqlDbType.VarChar, Convert.ToInt32(row[2].ToString()));
                                            ValorRetorno.Direction = ParameterDirection.Output;
                                            break;
                                        case "decimal":
                                            ValorRetorno = new SqlParameter(key, SqlDbType.Decimal, Convert.ToInt32(row[2].ToString()));
                                            ValorRetorno.Direction = ParameterDirection.Output;
                                            break;
                                        case "bit":
                                            ValorRetorno = new SqlParameter(key, SqlDbType.Decimal, Convert.ToInt32(row[2].ToString()));
                                            ValorRetorno.Direction = ParameterDirection.Output;
                                            break;
                                    }
                                    cmd.Parameters.Add(ValorRetorno);
                                    dictionaryReturned.Add(key, null);
                                    break;
                                }
                                else
                                {
                                    switch (row[1].ToString())
                                    {
                                        case "uniqueidentifier":
                                            cmd.Parameters.AddWithValue(key, value == null ? "" + Guid.Empty : "" + Guid.Parse(value.ToString()));
                                            break;
                                        case "int":
                                            cmd.Parameters.AddWithValue(key, Convert.ToInt32(value));
                                            break;
                                        case "nvarchar":
                                            cmd.Parameters.AddWithValue(key, Convert.ToString(value));
                                            break;
                                        case "varchar":
                                            cmd.Parameters.AddWithValue(key, Convert.ToString(value));
                                            break;
                                        case "decimal":
                                            cmd.Parameters.AddWithValue(key, value);
                                            break;
                                        case "bit":
                                            cmd.Parameters.AddWithValue(key, Convert.ToBoolean(value));
                                            break;
                                        case "datetime":
                                            cmd.Parameters.AddWithValue(key, Convert.ToDateTime(value));
                                            break;
                                    }
                                }

                            }
                        }
                    }
                }
                cnx.Open();
                cmd.ExecuteNonQuery();
                cnx.Close();
                if (returnvalue)
                {
                    Dictionary<string, object> getback = new Dictionary<string, object>();
                    getback.Clear();
                    foreach (KeyValuePair<string, object> item in dictionaryReturned)
                    {
                        getback.Add(item.Key, cmd.Parameters[item.Key].Value);
                    }
                    return getback;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                cnx.Close();
                throw e;

            }
        }

        /// <summary>
        /// METODO QUE EJECUTA UN STORE PROCEDURE CON X CANTIDAD DE PARAMETROS Y RETORNA X CANTIDAD DE OUTPUT
        /// UNO DE LOS PARAMETROS ES UN ARRAY, EL QUE SE GUARDARA EN LA TABLA DETALLE DE LA BD
        /// </summary>
        /// <param name="sp">NOMBRE DEL STORE PROCEDURE</param>
        /// <param name="param">PARAMETROS DEL STORE PROCEDURE</param>
        /// <param name="returnvalue">TRUE SI EL STORE PROCEDURE RETORNA X VALORES</param>
        /// <returns></returns>
        public static object execSpDataArray(string sp, Dictionary<string, object> param = null, bool returnvalue = false)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = GetSpInfo(sp);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = sp;
                cmd.Parameters.Clear();
                Dictionary<string, object> dictionaryReturned = returnvalue == true ? new Dictionary<string, object>() : null;
                if (param != null)
                {
                    dictionaryReturned.Clear();
                    foreach (KeyValuePair<string, object> keyobject in param)
                    {
                        string key = keyobject.Key.ToString();
                        object value = keyobject.Value == null ? null : keyobject.Value;
                        string type = keyobject.GetType().ToString();
                        foreach (DataRow row in dt.Rows)
                        {
                            if (row[0].ToString() == key)
                            {
                                if (key.Substring(key.Length - 3) == "OUT")
                                {
                                    SqlParameter ValorRetorno = null;
                                    switch (row[1].ToString())
                                    {
                                        case "int":
                                            ValorRetorno = new SqlParameter(key, SqlDbType.Int, Convert.ToInt32(row[2].ToString()));
                                            ValorRetorno.Direction = ParameterDirection.Output;
                                            break;
                                        case "nvarchar":
                                            ValorRetorno = new SqlParameter(key, SqlDbType.NVarChar, Convert.ToInt32(row[2].ToString()));
                                            ValorRetorno.Direction = ParameterDirection.Output;
                                            break;
                                        case "varchar":
                                            ValorRetorno = new SqlParameter(key, SqlDbType.VarChar, Convert.ToInt32(row[2].ToString()));
                                            ValorRetorno.Direction = ParameterDirection.Output;
                                            break;
                                        case "decimal":
                                            ValorRetorno = new SqlParameter(key, SqlDbType.Decimal, Convert.ToInt32(row[2].ToString()));
                                            ValorRetorno.Direction = ParameterDirection.Output;
                                            break;
                                        case "bit":
                                            ValorRetorno = new SqlParameter(key, SqlDbType.Decimal, Convert.ToInt32(row[2].ToString()));
                                            ValorRetorno.Direction = ParameterDirection.Output;
                                            break;
                                    }
                                    cmd.Parameters.Add(ValorRetorno);
                                    dictionaryReturned.Add(key, null);
                                    break;
                                }
                                else
                                {
                                    switch (row[1].ToString())
                                    {
                                        case "uniqueidentifier":
                                            cmd.Parameters.AddWithValue(key, value == null ? "" + Guid.Empty : "" + Guid.Parse(value.ToString()));
                                            break;
                                        case "int":
                                            cmd.Parameters.AddWithValue(key, Convert.ToInt32(value));
                                            break;
                                        case "nvarchar":
                                            cmd.Parameters.AddWithValue(key, Convert.ToString(value));
                                            break;
                                        case "varchar":
                                            cmd.Parameters.AddWithValue(key, Convert.ToString(value));
                                            break;
                                        case "decimal":
                                            cmd.Parameters.AddWithValue(key, value);
                                            break;
                                        case "bit":
                                            cmd.Parameters.AddWithValue(key, Convert.ToBoolean(value));
                                            break;
                                        case "datetime2":
                                            cmd.Parameters.AddWithValue(key, value);
                                            break;
                                        case "image":
                                            cmd.Parameters.AddWithValue(key, value);
                                            break;
                                        case "presupuestoDetalleDatosArray":
                                            cmd.Parameters.AddWithValue(key, value);
                                            break;
                                        case "proCuentasPorPagarAsientoDetalle":
                                            cmd.Parameters.AddWithValue(key, value);
                                            break;
                                        case "proRetencionesDetalle":
                                            cmd.Parameters.AddWithValue(key, value);
                                            break;
                                        case "datosAcademicosArray":
                                            cmd.Parameters.AddWithValue(key, value);
                                            break;
                                        case "datosIdiomasArray":
                                            cmd.Parameters.AddWithValue(key, value);
                                            break;
                                        case "datosExperienciasLaboralesArray":
                                            cmd.Parameters.AddWithValue(key, value);
                                            break;
                                        case "datosDocumentosArray":
                                            cmd.Parameters.AddWithValue(key, value);
                                            break;
                                        case "datosReferenciasArray":
                                            cmd.Parameters.AddWithValue(key, value);
                                            break;
                                        case "tblMenuPermisosDataArray":
                                            cmd.Parameters.Add(key, SqlDbType.Structured).Value = value;
                                            break;
                                        case "datosIrDetalleArray":
                                            cmd.Parameters.Add(key, SqlDbType.Structured).Value = value;
                                            break;
                                        case "comisionesVtaDetalleArray":
                                            cmd.Parameters.Add(key, SqlDbType.Structured).Value = value;
                                            break;
                                        case "datetime":
                                            cmd.Parameters.AddWithValue(key, Convert.ToDateTime(value));
                                            break;
                                    }
                                }

                            }
                        }
                    }
                }
                cnx.Open();
                cmd.ExecuteNonQuery();
                cnx.Close();
                if (returnvalue)
                {
                    Dictionary<string, object> getback = new Dictionary<string, object>();
                    getback.Clear();
                    foreach (KeyValuePair<string, object> item in dictionaryReturned)
                    {
                        getback.Add(item.Key, cmd.Parameters[item.Key].Value);
                    }
                    return getback;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                cnx.Close();
                throw e;

            }
        }

        /**********************************************************************************************/
        /*******************************************FIN execSP*****************************************/
        /**********************************************************************************************/
        #endregion

        #region FUNCIONES

        /// <summary>
        /// OBTIENE LA INFORMACION DE UN PROCEDIMIENTO ALMACENADO
        /// </summary>
        /// <param name="Sp">SENTENCIA SQL QUE EJECUTA UN PROCEDIMIENTO ALMACENADO</param>
        /// <returns></returns>
        private static DataTable GetSpInfo(string Sp)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            SqlDataAdapter dta = new SqlDataAdapter();
            string query = "Exec sp_help '" + Sp + "'";
            try
            {
                dta.SelectCommand = new SqlCommand(query, cnx);
                cnx.Open();
                dt.Clear();
                dta.Fill(ds);
                dt = ds.Tables[1];
                cnx.Close();
            }
            catch (Exception e)
            {
                if (cnx.State == System.Data.ConnectionState.Open)
                {
                    cnx.Close();
                }
                throw e;
            }
            return dt;


        }

        /**********************************************************************************************/
        /*******************************************FIN GETSPINFO**************************************/
        /**********************************************************************************************/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Table"></param>
        /// <param name="Opt"></param>
        /// <param name="vw"></param>
        /// <param name="Fields"></param>
        /// <param name="Sort"></param>
        /// <returns></returns>
        private static DataTable GetQuery(string Table, int Opt = 1, string vw = null, string Fields = null, int Sort = 0)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter dta = new SqlDataAdapter();
            string query = "";
            if (Opt == 1 && vw != null && Fields == null)
            {
                query = "SELECT * FROM " + vw;
            }
            else if (Opt == 1 && vw != null && Fields != null)
            {
                query = "SELECT * FROM " + vw + " WHERE " + Fields;
            }
            else if (Opt == 1 && vw == null && Fields != null)
            {
                query = "SELECT * FROM " + Table + " WHERE " + Fields;
            }
            else
            {
                query = "SELECT * FROM " + Table;
            }
            if (Opt == 2 && Fields == null && Sort == 0)
            {
                query = "SELECT * FROM " + Table + " ORDER BY " + Fields + " DESC";
            }
            if (Opt == 2 && Fields == null && Sort == 1)
            {
                query = "SELECT * FROM " + Table + " ORDER BY " + Fields + " ASC";
            }
            if (Opt == 2 && Fields != null && Sort == 0)
            {
                query = "SELECT * FROM " + Table + " ORDER BY " + Fields + " DESC";
            }
            if (Opt == 2 && Fields != null && Sort == 1)
            {
                query = "SELECT * FROM " + Table + " ORDER BY " + Fields + " ASC";
            }


            try
            {
                dta.SelectCommand = new SqlCommand(query, cnx);
                cnx.Open();
                dt.Clear();
                dta.Fill(dt);
                cnx.Close();
            }
            catch (Exception e)
            {
                if (cnx.State == System.Data.ConnectionState.Open)
                {
                    cnx.Close();
                }
                throw e;
            }
            return dt;
        }

        /**********************************************************************************************/
        /*******************************************FIN GETQUERY***************************************/
        /**********************************************************************************************/

        /// <summary>
        /// RETORNA EL ESQUEMA AL QUE PERTENECE EL OBJETO ObjPoco
        /// </summary>
        /// <param name="ObjPoco">TIPO DE OBJETO DE LA CLASE</param>
        /// <returns></returns>
        private static string GetSchema(Type ObjPoco)
        {

            string table = "";

            if (ObjPoco.FullName.Substring((ObjPoco.FullName.IndexOf(".") + 1)).IndexOf("+") > 0)
            {
                table = ObjPoco.FullName.Substring((ObjPoco.FullName.IndexOf(".") + 1)).Replace("+", ".");
            }
            else
            {
                table = "dbo." + ObjPoco.FullName.Substring((ObjPoco.FullName.IndexOf(".") + 1));
            }
            return table;
        }

        /**********************************************************************************************/
        /*******************************************FIN GETSCHEMA**************************************/
        /**********************************************************************************************/

        /// <summary>
        /// SERIALIZA UN DATATABLE
        /// </summary>
        /// <param name="dt">DATATABLE A SERIALIZAR</param>
        /// <param name="table">NOMBRE LA TABLA</param>
        /// <returns></returns>
        private static bool SerializeDt(DataTable dt, string table)
        {
            System.IO.FileStream fs = new System.IO.FileStream(SetName(table), System.IO.FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, dt);
            fs.Close();
            return true;
        }

        /**********************************************************************************************/
        /*******************************************FIN SERIALIZARDT***********************************/
        /**********************************************************************************************/

        /// <summary>
        /// DESERIALIZA UN DATATABLE
        /// </summary>
        /// <param name="table">NOMBRE LA TABLA</param>
        /// <returns></returns>
        private static DataTable DeSerializeDt(string table)
        {
            System.IO.FileStream fs = new System.IO.FileStream(SetName(table), System.IO.FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            DataTable dt = (DataTable)bf.Deserialize(fs);
            fs.Close();
            return dt;
        }

        /**********************************************************************************************/
        /*******************************************FIN DESIALIZARDT***********************************/
        /**********************************************************************************************/

        private static string SetName(string table)
        {
            string result = "C:\\sGP-DONOTDELETE\\" + CrypName(table) + "\\" + DateTime.Today.ToString("dd") + DateTime.Today.ToString("MM") + DateTime.Today.ToString("yy") + ".bin";
            return result;
        }

        private static bool GetExist(string Name)
        {
            string crname = CrypName(Name);
            string currentdir = Environment.CurrentDirectory;

            if (!System.IO.Directory.Exists("C:\\sGP-DONOTDELETE"))
            {
                System.IO.Directory.CreateDirectory("C:\\sGP-DONOTDELETE");
            }

            if (!System.IO.Directory.Exists("C:\\sGP-DONOTDELETE\\" + crname))
            {
                System.IO.Directory.CreateDirectory("C:\\sGP-DONOTDELETE\\" + crname);
            }

            if (!System.IO.File.Exists("C:\\sGP-DONOTDELETE\\" + crname + "\\" + DateTime.Today.ToString("dd") + DateTime.Today.ToString("MM") + DateTime.Today.ToString("yy") + ".bin"))
            {
                try
                {
                    string[] filePaths = System.IO.Directory.GetFiles("C:\\sGP-DONOTDELETE\\" + crname);
                    foreach (string filePath in filePaths)
                        System.IO.File.Delete(filePath);
                }
                catch (Exception e)
                {
                    throw e;
                }
                return false;
            }
            else
            {
                return true;
            }


        }

        private static string CrypName(string Name)
        {
            string result = string.Empty;
            byte[] encryted = System.Text.Encoding.ASCII.GetBytes(Name);
            result = Convert.ToBase64String(encryted);
            return result;
        }

        private static string DecrypName(string Name)
        {
            string result = string.Empty;
            byte[] decryted = Convert.FromBase64String(Name);
            result = System.Text.Encoding.ASCII.GetString(decryted);
            return result;
        }

        public static string Decryp(string Name)
        {
            string result = string.Empty;
            byte[] decryted = Convert.FromBase64String(Name);
            result = System.Text.Encoding.ASCII.GetString(decryted);
            return result;
        }

        /// <summary>
        /// RETORNA UN UNICO CAMPO DE LA SENTENCIA SQL
        /// </summary>
        /// <param name="Sql">SENTENCIA SQL, ESTA DEBE DE SER CREADA PARA RETORNAR UN SOLO CAMPO</param>
        /// <remarks>
        /// </remarks>
        public static object SingleData(string Sql)
        {
            object result = null;
            try
            {
                cmd.CommandText = Sql;
                cnx.Open();
                result = cmd.ExecuteScalar().ToString();
                cnx.Close();
                return result;
            }
            catch (Exception e)
            {
                if (cnx.State == System.Data.ConnectionState.Open)
                {
                    cnx.Close();
                    result = null;
                }
                return result;
                throw e;
            }
        }

        /// <summary>
        /// RETORNA UN UNICO CAMPO DE LA SENTENCIA SQL
        /// </summary>
        /// <param name="Sql">SENTENCIA SQL, ESTA DEBE DE SER CREADA PARA RETORNAR UN SOLO CAMPO</param>
        /// <remarks>
        /// </remarks>
        public static object SingleData(string Sql, string datatypeRetorned)
        {
            object result = null;
            try
            {
                cmd.CommandText = Sql;
                cnx.Open();
                switch (datatypeRetorned)
                {
                    case "string":
                        result = cmd.ExecuteScalar().ToString();
                        break;
                    case "arrayByte":
                        result = (byte[])cmd.ExecuteScalar();
                        break;
                }

                cnx.Close();
                return result;
            }
            catch (Exception e)
            {
                if (cnx.State == System.Data.ConnectionState.Open)
                {
                    cnx.Close();
                    result = null;
                }
                return result;
                throw e;
            }
        }

        /**********************************************************************************************/
        /*******************************************FIN SINGLEDATA*************************************/
        /**********************************************************************************************/

        /// <summary>
        /// CONVIERTE UN DATATABLE A UNA LISTA DEL TIPO DEL OBJETO objPoco
        /// </summary>
        /// <typeparam name="T">TIPO DE OBJETO</typeparam>
        /// <param name="objPoco"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<T> dtToList<T>(T objPoco, DataTable dt)
        {

            try
            {
                Type typeobjpoco = typeof(T);
                string tablename = GetSchema(typeobjpoco);
                PropertyDescriptorCollection PropiedadesObjPoco = TypeDescriptor.GetProperties(typeobjpoco);
                var listobj = new List<T>();
                int cont = 0;
                for (int i = 0; i < (dt.Rows.Count); i++)
                {
                    var obj = Activator.CreateInstance(typeof(T));
                    foreach (PropertyDescriptor prop in PropiedadesObjPoco)
                    {
                        if ((dt.Rows[i][cont].GetType()).ToString() == "System.DBNull")
                        {
                            prop.SetValue(obj, null);
                        }
                        else
                        {
                            prop.SetValue(obj, dt.Rows[i][cont]);
                        }

                        cont++;
                    }
                    listobj.Add((T)Convert.ChangeType(obj, typeof(T)));
                    cont = 0;
                }
                return listobj;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        /**********************************************************************************************/
        /*******************************************FIN dtToList***************************************/
        /**********************************************************************************************/
        #endregion

    }
}
