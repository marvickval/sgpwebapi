﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SgpWebApi.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public List<EL.Cnf.usuarios> Get()
        {
            var data = new BL.MetodoConf().getListUsuarios();
            //return new string[] { "value1", "value2" };
            return data;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
