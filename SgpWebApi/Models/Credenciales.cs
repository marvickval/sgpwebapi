﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SgpWebApi.Models
{
    public class Credenciales
    {
        public string usuario { get; set; }
        public string password { get; set; }
    }
}