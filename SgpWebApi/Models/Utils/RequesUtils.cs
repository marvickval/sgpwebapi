﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace SgpWebApi.Models.Utils
{
    public class RequesUtils
    {
        public Reply reply { get; set; }

        public RequesUtils()
        {
            reply = new Reply();
        }

        public Reply Execute<T>(string url, string metodo, T objectRequest)
        {
            reply.resultado = 0;
            string resultado = "";
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = JsonConvert.SerializeObject(objectRequest);

                WebRequest request = WebRequest.Create(url);
                request.Method = metodo;
                request.PreAuthenticate = true;
                request.ContentType = "application/json;charset=utf-8";
                request.Timeout = 60000;

                using(var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)request.GetResponse();
                using(var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    resultado = streamReader.ReadToEnd();
                }

                reply = JsonConvert.DeserializeObject<Reply>(resultado);
                
            }
            catch(TimeoutException te)
            {
                reply.mensaje = "Servidor sin respuesta";
            }
            catch (Exception e)
            {
                reply.mensaje = "Ocurrió un error al procesar los datos";
            }
            return reply;
        }

    }
}