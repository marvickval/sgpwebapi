﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SgpWebApi
{
    public class Reply
    {
        public string mensaje{ get; set; }
        public int resultado { get; set; }
        public object data { get; set; }
    }
}